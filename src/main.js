import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import vuetify from './plugins/vuetify';
import './sass/style.scss';

Vue.config.productionTip = false;

window.mexApp = new Vue({
  vuetify,
  data: {
	isNewVersionAvailable: false,
	versionWorker: null
  },
  render: h => h(App)
}).$mount('#app');